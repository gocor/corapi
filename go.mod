module gitlab.com/gocor/corapi

go 1.16

require (
	github.com/pkg/errors v0.9.1
	gitlab.com/gocor/corctx v1.0.0
)
